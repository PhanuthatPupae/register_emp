import React from 'react';
import Select from 'react-select';
import { DropdownStyleWapper } from './Style';

const customStyles = {
  option: (provided, state) => ({ ...provided }),
  control: (base, state) => ({
    ...base,
    boxShadow: 'none',
    width: 120,
    minHeight: 30,
    height: 30,
    borderColor: 'black',
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    height: '30px',
    padding: '0 6px',
  }),
  input: (provided, state) => ({
    ...provided,
    margin: '0px',
    paddingBottom: 5,
  }),
  indicatorSeparator: (state) => ({
    display: 'none',
  }),
  indicatorsContainer: (provided, state) => ({
    ...provided,
    height: '30px',
    color: 'black',
  }),
};

export function Dropdown({
  value = {},
  options = [],
  onChange = () => {},
  placeholder = '',
  title = '',
  isRequired = false,
  error = '',
}) {
  return (
    <>
      <DropdownStyleWapper>
        <span>
          {title}:{isRequired && <span style={{ color: 'red' }}>*</span>}
        </span>
        <Select
          placeholder={<div>{placeholder}</div>}
          onChange={onChange}
          value={value}
          options={options}
          styles={customStyles}
          components={{
            IndicatorSeparator: () => null,
          }}
        />
      </DropdownStyleWapper>
      {!!error && isRequired && (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <span style={{ fontSize: 10, color: 'red' }}>{error.value}</span>
        </div>
      )}
    </>
  );
}
