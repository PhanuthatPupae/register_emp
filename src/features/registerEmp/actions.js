export const REGISTER_EMP_REQUEST = 'REGISTER_EMP_REQUEST';
export const REGISTER_EMP_SUCCESS = 'REGISTER_EMP_SUCCESS';
export const REGISTER_EMP_FAIL = 'REGISTER_EMP_REQUEST';

export const DELETE_EMP_REQUEST = 'DELETE_EMP_REQUEST';
export const DELETE_EMP_SUCCESS = 'DELETE_EMP_SUCCESS';
export const DELETE_EMP_FAIL = 'DELETE_EMP_FAIL';

export const EDIT_EMP_REQUEST = 'EDIT_EMP_REQUEST';
export const EDIT_EMP_SUCCESS = 'EDIT_EMP_SUCCESS';
export const EDIT_EMP_FAIL = 'EDIT_EMP_FAIL';

export const GET_EMP_REQUEST = 'GET_EMP_REQUEST';
export const GET_EMP_SUCESS = 'GET_EMP_SUCESS';
export const GET_EMP_FAIL = 'GET_EMP_FAIL';

export const DELETE_MULTIPLE_EMP_REQUEST = 'DELETE_MULTIPLE_EMP_REQUEST';
export const DELETE_MULTIPLE_EMP_SUCCESS = 'DELETE_MULTIPLE_EMP_SUCCESS';
export const DELETE_MULTIPLE_EMP_FAIL = 'DELETE_MULTIPLE_EMP_FAIL';

export const SELETC_EMP = 'SELETC_EMP';
export const CLEAR_SELETC_EMP = 'CLEAR_SELETC_EMP';
export const registerEmp = (req) => async (dispatch) => {
  try {
    dispatch({ type: REGISTER_EMP_REQUEST });
    const res = await new Promise(function (resolve) {
      setTimeout(resolve({ status: 200, message: 'success' }), 2000);
    });
    if (res && res.status === 200) {
      dispatch({
        type: REGISTER_EMP_SUCCESS,
        payload: {
          emp: req,
        },
      });
    }
  } catch (error) {
    dispatch({
      type: REGISTER_EMP_FAIL,
    });
  }
};

export const deleteEmpByID = (id) => async (dispatch) => {
  try {
    dispatch({ type: DELETE_EMP_REQUEST });
    const res = await new Promise(function (resolve) {
      setTimeout(resolve({ status: 200, message: 'success' }), 2000);
    });
    if (res && res.status === 200) {
      dispatch({
        type: DELETE_EMP_SUCCESS,
        payload: {
          id: id,
        },
      });
    }
  } catch (error) {
    dispatch({
      type: DELETE_EMP_FAIL,
    });
  }
};

export const deleteEmpAll =
  (idList = []) =>
  async (dispatch) => {
    try {
      dispatch({ type: DELETE_MULTIPLE_EMP_REQUEST });
      const res = await new Promise(function (resolve) {
        setTimeout(resolve({ status: 200, message: 'success' }), 2000);
      });
      if (res && res.status === 200) {
        dispatch({
          type: DELETE_MULTIPLE_EMP_SUCCESS,
          payload: {
            idList: idList,
          },
        });
      }
    } catch (error) {
      dispatch({
        type: DELETE_MULTIPLE_EMP_FAIL,
      });
    }
  };

export const onEditEmpByID = (empData) => async (dispatch) => {
  try {
    dispatch({ type: EDIT_EMP_REQUEST });
    const res = await new Promise(function (resolve) {
      setTimeout(resolve({ status: 200, message: 'success' }), 2000);
    });
    if (res && res.status === 200) {
      dispatch({
        type: EDIT_EMP_SUCCESS,
        payload: {
          empData: empData,
        },
      });
    }
  } catch (error) {
    dispatch({
      type: EDIT_EMP_FAIL,
    });
  }
};

export const selectEmp =
  (selectEmp = {}) =>
  (dispatch) => {
    dispatch({
      type: SELETC_EMP,
      payload: { selectEmp: selectEmp },
    });
  };

export const clearIsEdit = () => (dispatch) => {
  dispatch({
    type: CLEAR_SELETC_EMP,
  });
};
