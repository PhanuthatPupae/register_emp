import styled from 'styled-components';

export const PaginateStyleWapper = styled.div`
  display: flex;
  .paginationBttns {
    width: 80%;
    height: 30px;
    list-style: none;
    display: flex;
    justify-content: center;
  }

  .paginationBttns a {
    padding: 5px;
    margin: 8px;
    cursor: pointer;
  }

  .paginationBttns a:hover {
    color: white;
  }

  .paginationActive a {
    color: red;
  }

  .paginationDisabled a {
    color: grey;
  }
`;
