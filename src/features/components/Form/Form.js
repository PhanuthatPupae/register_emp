import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import { Dropdown } from '../../../components/dropdown/Dropdown';
import {
  CountryCode,
  Nationlity,
  TitleOption,
} from '../../registerEmp/constant';
import { InputWithTitle } from '../../../components/inputWithTitle/InputWithTitle';
import { DatePicker } from '../../../components/datePicker/DatePicker';
import { CitiZenID } from '../../../components/citizenID/CitiZenID';
import { GenderRadio } from '../../../components/genderRadio/GenderRadio';
import { MobileNumber } from '../../../components/mobileNumber/MobileNumber';
import { FormStyleWapper } from './Style';
import Button from '@material-ui/core/Button';
import { useDispatch, useSelector } from 'react-redux';
import { onEditEmpByID, registerEmp } from '../../registerEmp/actions';
import * as Yup from 'yup';
export const trimmedString = (str = '') => {
  return str.trim();
};

export const Form = (props) => {
  const dispatch = useDispatch();
  const store = useSelector(({ register }) => {
    return {
      empList: register.empList,
      selectEmp: register.selectEmp,
      isEdit: register.isEdit,
    };
  });

  const { empList, selectEmp, isEdit } = store;
  const [empData, setEmpData] = useState({
    title: '',
    firstName: '',
    lastName: '',
    birthday: '',
    nationality: '',
    citizenID: '',
    gender: '',
    phonNumber: '',
    passportNo: '',
    expectedSalary: '',
    countryCode: '',
  });

  useEffect(() => {
    initEmpForm();
  }, [selectEmp]);

  const initEmpForm = () => {
    if (selectEmp) {
      setEmpData(selectEmp);
    }
  };

  const validateSchema = () => {
    const schema = Yup.object().shape({
      firstName: Yup.string().required('firstName  required'),
      lastName: Yup.string().required('lastName  required'),
      birthday: Yup.string().required('birthday  required'),
      phonNumber: Yup.string().required('phonNumber  required'),
      expectedSalary: Yup.string().required('expected salary  required'),
      title: Yup.object().shape({
        label: Yup.string().required('title  required'),
        value: Yup.string().required('title  required'),
      }),
    });
    return schema;
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: selectEmp,
    onSubmit: (values, formikActions) => {
      onSubmitForm(values);
    },
    validationSchema: validateSchema,
  });

  const { values, handleChange, setFieldValue, errors } = formik;

  const handleCitizenID = (values = {}, formikSetFieldValue = () => {}) => {
    if (
      values &&
      values.ssn1 &&
      values.ssn2 &&
      values.ssn3 &&
      values.ssn4 &&
      values.ssn5
    ) {
      const citizenID =
        values.ssn1 + values.ssn2 + values.ssn3 + values.ssn4 + values.ssn5;
      formikSetFieldValue('citizenID', citizenID);
    }
  };

  const onSubmitForm = (values = {}) => {
    if (isEdit) {
      onEditEmp(values);
    } else {
      onRegister(values);
    }
  };

  const onEditEmp = (values = {}) => {
    dispatch(onEditEmpByID(values));
  };

  const onRegister = (values = {}) => {
    const DEFAULT_COUNTTY_CODE = '+66';
    const req = {
      ...values,
      id: empList.length + 1,
      countryCode: values.countryCode
        ? values.countryCode
        : DEFAULT_COUNTTY_CODE,
    };
    dispatch(registerEmp(req));
  };
  return (
    <FormStyleWapper>
      <div className='container-wrap'>
        <div>
          <Dropdown
            title='Title'
            options={TitleOption}
            isRequired
            onChange={(val) => {
              setFieldValue('title', val);
            }}
            value={values.title}
            error={errors.title}
          />
        </div>
        <div>
          <InputWithTitle
            title='FirstName'
            isRequired
            onChange={handleChange('firstName')}
            value={values.firstName}
            error={errors.firstName}
          />
        </div>
        <div>
          <InputWithTitle
            title='LastName'
            isRequired
            onChange={handleChange('lastName')}
            value={values.lastName}
            error={errors.lastName}
          />
        </div>
      </div>
      <div className='container-wrap' style={{ marginTop: 16 }}>
        <div>
          <DatePicker
            title='Birthday'
            isRequired
            onChange={(val) => {
              setFieldValue('birthday', val);
            }}
            selected={values.birthday ? new Date(values.birthday) : new Date()}
            error={errors.birthday}
            // value={values.birthday}
          />
        </div>
        <div>
          <Dropdown
            title='Nationlity'
            options={Nationlity}
            onChange={(val) => {
              setFieldValue('nationality', val);
            }}
            value={values.nationality}
          />
        </div>
      </div>
      <div className='container-wrap' style={{ marginTop: 16 }}>
        <CitiZenID onChange={(val) => handleCitizenID(val, setFieldValue)} />
      </div>
      <div className='container-wrap' style={{ marginTop: 16 }}>
        <GenderRadio
          onChange={(event) => {
            setFieldValue('gender', event.target.value);
          }}
          value={values.gender}
        />
      </div>
      <div className='container-wrap' style={{ marginTop: 16 }}>
        <MobileNumber
          title='Mobile Phone'
          isRequired
          options={CountryCode}
          onChange={(event) => {
            setFieldValue('phonNumber', event.target.value);
          }}
          onSelect={(val) => {
            setFieldValue('countryCode', val);
          }}
          selectValue={values.countryCode}
          value={values.phonNumber}
          error={errors.phonNumber}
        />
      </div>
      <div className='container-wrap' style={{ marginTop: 16 }}>
        <InputWithTitle
          title='Passport No'
          onChange={handleChange('passportNo')}
          value={values.passportNo}
        />
      </div>
      <div
        className='container-wrap'
        style={{ marginTop: 16, justifyContent: 'space-between' }}
      >
        <div>
          <InputWithTitle
            title='Expected Salary'
            isRequired
            isShowText
            text='THB'
            onChange={handleChange('expectedSalary')}
            value={values.expectedSalary}
            error={errors.expectedSalary}
          />
        </div>

        <div>
          <Button
            variant='contained'
            color='primary'
            onClick={() => {
              formik.submitForm();
            }}
          >
            Submit
          </Button>
        </div>
      </div>
    </FormStyleWapper>
  );
};
