import React from 'react';
import { CitiZenIDStyleWapper } from './Style';

const numOfFields = 5;

const useSSNFields = () => {
  const [ssnValues, setValue] = React.useState({
    ssn1: '',
    ssn2: '',
    ssn3: '',
    ssn4: '',
    ssn5: '',
  });
  return {
    handleChange: (e, callback = () => {}) => {
      const { maxLength, value, name, minLength } = e.target;
      const [fieldName, fieldIndex] = name.split('-');
      if (value.length >= maxLength) {
        if (parseInt(fieldIndex, 10) < numOfFields) {
          const nextSibling = document.querySelector(
            `input[name=ssn-${parseInt(fieldIndex, 10) + 1}]`
          );

          if (nextSibling !== null) {
            nextSibling.focus();
          }
        }
      } else if (value.length <= minLength) {
        if (parseInt(fieldIndex, 10) <= numOfFields) {
          const backSibling = document.querySelector(
            `input[name=ssn-${parseInt(fieldIndex, 10) - 1}]`
          );
          if (backSibling !== null) {
            backSibling.focus();
          }
        }
      }

      callback({ ...ssnValues, [`ssn${fieldIndex}`]: value });

      setValue({
        ...ssnValues,
        [`ssn${fieldIndex}`]: value,
      });
    },
    values: ssnValues,
  };
};

export function CitiZenID({
  title = 'CitiZenID',
  isRequired = false,
  onChange = () => {},
}) {
  const { handleChange, values } = useSSNFields();
  const onInputChange = (e) => {
    handleChange(e, onChange);
  };
  return (
    <CitiZenIDStyleWapper>
      <span>
        {title}:{isRequired && <span style={{ color: 'red' }}>*</span>}
      </span>
      <div className='input-citizen-container'>
        <input
          style={{ width: 10 }}
          name='ssn-1'
          onChange={onInputChange}
          maxLength={1}
          minLength={0}
        />
        <span>-</span>
        <input
          style={{ width: 40 }}
          name='ssn-2'
          onChange={onInputChange}
          maxLength={4}
          minLength={0}
        />
        <span>-</span>
        <input
          style={{ width: 60 }}
          name='ssn-3'
          onChange={onInputChange}
          maxLength={5}
          minLength={0}
        />
        <span>-</span>
        <input
          style={{ width: 20 }}
          name='ssn-4'
          onChange={onInputChange}
          maxLength={2}
          minLength={0}
        />
        <span>-</span>
        <input
          style={{ width: 10 }}
          name='ssn-5'
          onChange={onInputChange}
          maxLength={1}
          minLength={0}
        />
      </div>
    </CitiZenIDStyleWapper>
  );
}
