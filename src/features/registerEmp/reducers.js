import * as act from './actions';
const DEFAULR_SELETC_EMP = {
  title: '',
  firstName: '',
  lastName: '',
  birthday: '',
  nationality: '',
  citizenID: '',
  gender: 'male',
  phonNumber: '',
  passportNo: '',
  expectedSalary: '',
  countryCode: '',
};

const initialState = {
  empList: [],
  selectEmp: DEFAULR_SELETC_EMP,
  isEdit: false,
};

const removeEmpByid = (empList = [], id = 0) => {
  const index = empList.findIndex((element) => {
    return element.id === id;
  });
  if (index !== -1) {
    empList.splice(index, 1);
  }
  return [...empList];
};

const removeEmpByListID = (empList = [], idList = []) => {
  const empAfterRemove = empList.filter((item) => {
    const findID = idList.find((id) => id === item.id);
    if (!findID) {
      return { ...item };
    }
  });
  return [...empAfterRemove];
};

const editEmpByID = (empList = [], empData = {}) => {
  if (empData) {
    return empList.map((item) => {
      if (item.id === empData.id) {
        return { ...empData };
      } else {
        return { ...item };
      }
    });
  } else {
    return [...empList];
  }
};

// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, action) => {
  switch (action.type) {
    case act.REGISTER_EMP_REQUEST:
      return {
        ...state,
        registerEmpRequest: true,
        registerEmpError: false,
      };
    case act.REGISTER_EMP_SUCCESS:
      return {
        ...state,
        registerEmpRequest: false,
        registerEmpError: false,
        empList: [...state.empList, action.payload.emp],
      };
    case act.REGISTER_EMP_FAIL:
      return {
        ...state,
        registerEmpRequest: false,
        registerEmpError: true,
      };
    case act.DELETE_EMP_REQUEST:
      return {
        ...state,
        deleteEmpRequest: true,
        deleteEmpError: false,
      };
    case act.DELETE_EMP_SUCCESS:
      return {
        ...state,
        deleteEmpRequest: false,
        deleteEmpError: false,
        empList: removeEmpByid(state.empList, action.payload.id),
      };
    case act.DELETE_EMP_FAIL:
      return {
        ...state,
        deleteEmpRequest: false,
        deleteEmpError: true,
      };
    case act.DELETE_MULTIPLE_EMP_REQUEST:
      return {
        ...state,
        deleteEmpMultipleRequest: true,
        deleteEmpMultipleError: false,
      };
    case act.DELETE_MULTIPLE_EMP_SUCCESS:
      return {
        ...state,
        deleteEmpMultipleRequest: false,
        deleteEmpMultipleError: false,
        empList: removeEmpByListID(state.empList, action.payload.idList),
      };
    case act.DELETE_MULTIPLE_EMP_FAIL:
      return {
        ...state,
        deleteEmpMultipleRequest: false,
        deleteEmpMultipleError: true,
      };
    case act.EDIT_EMP_REQUEST:
      return {
        ...state,
        editEmpMultipleRequest: true,
        editEmpMultipleError: false,
      };
    case act.EDIT_EMP_SUCCESS:
      return {
        ...state,
        editEmpMultipleRequest: false,
        editEmpMultipleError: false,
        empList: editEmpByID(state.empList, action.payload.empData),
      };
    case act.EDIT_EMP_FAIL:
      return {
        ...state,
        editEmpMultipleRequest: false,
        editEmpMultipleError: true,
      };
    case act.SELETC_EMP:
      return {
        ...state,
        selectEmp: action.payload.selectEmp,
        isEdit: true,
      };
    case act.CLEAR_SELETC_EMP:
      return {
        ...state,
        isEdit: false,
        selectEmp: {
          title: '',
          firstName: '',
          lastName: '',
          birthday: '',
          nationality: '',
          citizenID: '',
          gender: 'male',
          phonNumber: '',
          passportNo: '',
          expectedSalary: '',
          countryCode: '',
        },
      };
    default:
      return state;
  }
};
