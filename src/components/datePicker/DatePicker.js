import React from 'react';
import DatePickers from 'react-datepicker';
import { DatePickerStyleWapper } from './Style';
export function DatePicker({
  title = '',
  isRequired = false,
  selected = new Date(),
  onChange = () => {},
  value = {},
  error = '',
}) {
  return (
    <>
      <DatePickerStyleWapper>
        <span>
          {title}:{isRequired && <span style={{ color: 'red' }}>*</span>}
        </span>
        <DatePickers
          className='select-container'
          showYearDropdown
          showMonthDropdown
          dropdownMode='select'
          dateFormat='d/MM/yyyy'
          selected={selected}
          onChange={onChange}
          value={value}
        />
      </DatePickerStyleWapper>
      {!!error && isRequired && (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <span style={{ fontSize: 10, color: 'red' }}>{error}</span>
        </div>
      )}
    </>
  );
}
