import styled from 'styled-components';

export const InputStyleWapper = styled.div`
  display: flex;
  width: 100%;
  gap: 16px;
  .input-wapper {
    width: 200px;
    height: 25px;
    border-radius: 5px;
    border: 1px solid;
  }
`;
