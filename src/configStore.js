import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import ReduxThunk from 'redux-thunk';
import ReduxLogger from 'redux-logger';
import rootReducer from './reducer';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

// eslint-disable-next-line import/no-anonymous-default-export
export default (initialState) => {
  const middleware = [ReduxThunk, ReduxLogger];

  const store = createStore(
    persistedReducer,
    initialState,
    applyMiddleware(...middleware)
  );

  const persistor = persistStore(store);

  return { store, persistor };
};
