import styled from 'styled-components';

export const DropdownStyleWapper = styled.div`
  display: flex;
  width: 100%;
  gap: 16px;
`;
