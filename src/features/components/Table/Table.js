import React, { useState, useEffect } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import ReactPaginate from 'react-paginate';
import { PaginateStyleWapper } from './Style';
import { useDispatch, useSelector } from 'react-redux';
import {
  clearIsEdit,
  deleteEmpAll,
  deleteEmpByID,
  selectEmp,
} from '../../registerEmp/actions';
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

export function TableRegister() {
  const classes = useStyles();
  const [empList, setEmpList] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [isSelectedAll, setIsSelectedAll] = useState(false);
  const store = useSelector(({ register }) => {
    return {
      empList: register.empList,
    };
  });

  const dispatch = useDispatch();

  useEffect(() => {
    initEmpList();
  }, []);

  useEffect(() => {
    initEmpList();
  }, [store.empList, currentPage, dispatch]);

  const initEmpList = () => {
    const PER_PAGE = 10;
    const offset = currentPage * PER_PAGE;
    const currentPageData = store.empList
      .slice(offset, offset + PER_PAGE)
      .map((item) => {
        return { ...item, isItemSelected: false };
      });
    const pageCount = Math.ceil(store.empList.length / PER_PAGE);
    setPageCount(pageCount);
    setEmpList(currentPageData);
    clearSelectAll();
    dispatch(clearIsEdit());
  };

  const handlePageClick = ({ selected: selectedPage }) => {
    setCurrentPage(selectedPage);
    clearSelectAll();
    dispatch(clearIsEdit());
  };

  const handleSelect = (id = 0) => {
    const empSelect = empList.map((item) => {
      if (item.id === id) {
        return { ...item, isItemSelected: !item.isItemSelected };
      } else {
        return { ...item };
      }
    });
    setEmpList(empSelect);
  };

  const handleSelectAll = () => {
    const PER_PAGE = 10;
    const offset = currentPage * PER_PAGE;
    const currentPageData = store.empList
      .slice(offset, offset + PER_PAGE)
      .map((item) => {
        return { ...item, isItemSelected: !isSelectedAll };
      });
    setEmpList(currentPageData);
    setIsSelectedAll(!isSelectedAll);
  };

  const onDeleteByID = (id = 0) => {
    if (id) {
      dispatch(deleteEmpByID(id));
    }
  };

  const onDeleteMultipleSelect = () => {
    const listID = empList.map((item) => {
      if (item.isItemSelected) {
        return item.id;
      }
    });
    if (listID && listID.length > 0) {
      dispatch(deleteEmpAll(listID));
    }
  };

  const onClickEdit = (empData = {}) => {
    dispatch(selectEmp(empData));
  };

  const clearSelectAll = () => {
    if (isSelectedAll) {
      setIsSelectedAll(false);
    }
  };
  return (
    <>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Checkbox
            checked={isSelectedAll}
            onClick={() => {
              handleSelectAll();
            }}
          />
          <div>
            <span>Select All</span>
          </div>
          <div style={{ marginLeft: 16 }}>
            <Button
              variant='contained'
              color='secondary'
              onClick={() => {
                onDeleteMultipleSelect();
              }}
            >
              DELETE
            </Button>
          </div>
        </div>
        <PaginateStyleWapper>
          <ReactPaginate
            previousLabel={'Previous'}
            nextLabel={'Next'}
            pageCount={pageCount}
            onPageChange={handlePageClick}
            containerClassName={'paginationBttns'}
            previousLinkClassName={'previousBttn'}
            nextLinkClassName={'nextBttn'}
            disabledClassName={'paginationDisabled'}
            activeClassName={'paginationActive'}
          />
        </PaginateStyleWapper>
      </div>

      <TableContainer component={Paper}>
        <Table
          className={classes.table}
          size='small'
          aria-label='a dense table'
        >
          <TableHead>
            <TableRow>
              <StyledTableCell></StyledTableCell>
              <StyledTableCell>NAME</StyledTableCell>
              <StyledTableCell align='right'>GENDER</StyledTableCell>
              <StyledTableCell align='right'>MOBILE PHONE</StyledTableCell>
              <StyledTableCell align='right'>NATIONALITY</StyledTableCell>
              <StyledTableCell align='right'></StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {empList.map((row) => (
              <TableRow key={row.firstName}>
                <TableCell padding='checkbox'>
                  <Checkbox
                    checked={row.isItemSelected}
                    inputProps={{ 'aria-labelledby': row.id }}
                    onClick={(event) => {
                      handleSelect(row.id);
                    }}
                  />
                </TableCell>
                <TableCell component='th' scope='row'>
                  {row.firstName + ' ' + row.lastName}
                </TableCell>
                <TableCell align='center'>{row.gender}</TableCell>
                <TableCell align='center'>{row.phonNumber}</TableCell>
                <TableCell align='center'>{row.nationality?.value}</TableCell>
                <TableCell align='center'>
                  <Button
                    color='primary'
                    onClick={() => {
                      onClickEdit(row);
                    }}
                  >
                    Edit
                  </Button>
                  /
                  <Button
                    color='secondary'
                    onClick={() => {
                      onDeleteByID(row.id);
                    }}
                  >
                    Delete
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
