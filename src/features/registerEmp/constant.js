export const TitleOption = [
  { value: 'Mr.', label: 'Mr.' },
  { value: 'Mrs.', label: 'Mrs.' },
  { value: 'Ms.', label: 'Ms.' },
];

export const Nationlity = [{ value: 'Thai', label: 'Thai' }];
export const CountryCode = [{ value: '+66', label: '+66' }];
