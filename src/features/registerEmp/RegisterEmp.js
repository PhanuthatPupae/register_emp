import React from 'react';
import { TableRegister } from '../components/Table/Table';
import { Form } from '../components/Form/Form';
import { RegisterEmpStyleWapper } from './Style';
export function RegisterEmp(props) {
  return (
    <RegisterEmpStyleWapper>
      <Form />
      <div>
        <TableRegister />
      </div>
    </RegisterEmpStyleWapper>
  );
}

RegisterEmp.propTypes = {};
