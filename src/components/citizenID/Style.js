import styled from 'styled-components';

export const CitiZenIDStyleWapper = styled.div`
  display: flex;
  width: 100%;
  gap: 16px;
  .input-citizen-container {
    gap: 5px;
    display: flex;
  }
`;
