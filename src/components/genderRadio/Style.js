import styled from 'styled-components';

export const GenderRadioStyleWapper = styled.div`
  display: flex;
  width: 100%;
  gap: 16px;
  align-items: center;
`;
