import React from 'react';
import { GenderRadioStyleWapper } from './Style';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

export function GenderRadio({
  value = 'male',
  options = [],
  onChange = () => {},
  placeholder = '',
  title = 'Gender',
  isRequired = false,
}) {
  return (
    <GenderRadioStyleWapper>
      <span>
        {title}:{isRequired && <span style={{ color: 'red' }}>*</span>}
      </span>
      <RadioGroup
        row
        aria-label='gender'
        name='gender1'
        // value={value ? value : 'male'}
        onChange={onChange}
        defaultValue={value}
      >
        <FormControlLabel value='female' control={<Radio />} label='Female' />
        <FormControlLabel value='male' control={<Radio />} label='Male' />
        <FormControlLabel value='Unisex' control={<Radio />} label='Unisex' />
      </RadioGroup>
    </GenderRadioStyleWapper>
  );
}
